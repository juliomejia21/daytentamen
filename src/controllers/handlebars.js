//function to change the time time format
const {format} = require('timeago.js');

const helpers= {};
helpers.timeago=(timestamp)=>{
    return format(timestamp);
}
module.exports=helpers;